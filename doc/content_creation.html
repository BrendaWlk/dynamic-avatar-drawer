---
layout: page
title: Content creation guide
permalink: /content_creation.html
css: syntax
group: Content Creation
---
<div class="toc">
    <a class="toc-link toch2" href="#scale">Understanding Scalability</a>
    <a class="toc-link toch2" href="#clothing">Clothing</a>
    <a class="toc-link toch2" href="#tips">Tips</a>
    <p class="toc-caption"></p>
    <p class="toc-toggle">toggle TOC (ctrl + &#8660;)</p>
</div>

<div class="block">
<div class="text-block">
    <p>
        An exciting part of DAD is that it's built so you can easily extend it to create your own assets
        in the form of clothing and body parts. This is an advanced usage of the tool, but
        any large scale project will probably want to go beyond the standard assets provided.
    </p>
    <p>
        If you do create custom assets, you retain copyright of it, but I encourage you to submit
        a merge request for it back to the master project so others could use your content.
    </p>
    <p>
        You should be familiar with git. The remote repository for this project is on gitlab, which
        is where you should also have your version of the project. The recommended workflow is:
    </p>
    <ol>
        <li>fork the project on <a href="https://gitlab.com/PerplexedPeach/dynamic-avatar-drawer">gitlab</a></li>
        <li>look at <a href="https://johnsonzhong.me/drawpoint-demo/">interactive drawpoints demo</a> to see how you can draw</li>
        <li>look at existing code under the <code>src</code> directory to see where to insert your content</li>
        <li>develop locally (commit) following the instructions in the README so you're eventually running <code>npm run start</code></li>
        <li>push to your fork on gitlab</li>
        <li>submit a merge request, see <a href="https://docs.gitlab.com/ee/workflow/forking_workflow.html">forking workflow</a></li>
    </ol>
</div>

<h2 class="anchor">Understanding Scalability <a class="anchor-link"
                                                title="permalink to section"
                                                href="#scale"
                                                name="scale">&para;</a>
</h2>
<div class="text-block">
    <p>
        It's important to understand how DAD keeps everything to scale with body dimensions.
        The fundamental concept here is <b>drawpoints</b>. These are points exported by
        the draw method that are each in a format like:
    </p>
</div>
{% highlight javascript %}
ex.thumb.out = {
    x: units in cm relative to center of body, with center = 0,
    y: units in cm relative to floor, with floor = 0,
    cp1: optional control point (with its own x and y)
    cp2: optional second control point
}
{% endhighlight %}

<div class="text-block">
    <p>
        Rendering involves connecting sequences of drawpoints using lines, and quadratic and bezier curves.
        You can see the drawpoints and how they move in the tester provided (by toggling it on via button).
    </p>
    <p>
        You connect draw points with the da.drawPoints method, which smartly decide whether to connect using
        line, quadratic, or bezier curve depending on how many control points exist on the draw point.
        See <strong><a href="https://johnsonzhong.me/drawpoint-demo/">interactive drawpoint demo</a></strong>
        to see how to manipulate and create drawpoints. You can drag the drawpoints around and see how
        the code should change to produce it.
    </p>
</div>
{% highlight javascript %}
// these are the points of a human leg
var humanLegDrawpoints = [
    da.extractPoint(ex.hip),
    ex.thigh.out,
    ex.knee.out,
    ex.calf.out,
    ex.ankle.out,
    // insert the sequence of child draw points (if any exists)
    {child: true},
    ex.ankle.in,
    ex.calf.in,
    ex.knee.in,
    ex.knee.intop,
    ex.thigh.in,
    ex.thigh.top,
];

// your body parts just have to export the sequence of draw points
// the drawpoints are parsed and reordered internally, but basically it's called like
da.drawPoint(ctx, ...humanLegDrawPoints);
{% endhighlight %}

<div class="text-block">
    <p>
        cp1 and cp2 are for the curve going into the drawpoint. Having only cp1 would make it a quadratic curve,
        while having both cp1 and cp2 makes it a cubic bezier curve.
    </p>
</div>
<img src="http://i.imgur.com/GYzYdwI.png"/>

<div class="text-block">
    <p>
        You can see the control points for any drawpoint by wrapping it with
        <code>da.tracePoint(ex.ankle.out, [options]))</code> when passing it to drawPoints.
        A breakdown of the options you can pass in:
    </p>
</div>
{% highlight javascript %}
 const options = {
    controlLine: {
        color: "rgb(200,100,100)",
        width: 0.5
    },
    point: {
        // what to colour control points and the start point stroke
        color: "rgb(200,50,50)",
        // what to colour the end point stroke
        destinationColor: "#000",
        // what to fill the points with
        fill: "white",
        // the thickness of the stroke for points
        width: 1,
        // how big the points should be
        radius: 1
    },
};
{% endhighlight %}

    <h2 class="anchor">Clothing <a class="anchor-link"
                               title="permalink to section"
                               href="#clothing"
                                   name="clothing">&para;</a></h2>
    <div class="text-block">
        <p>
            Clothing is split into <code>Clothing</code> and <code>ClothingPart</code> classes.
            <code>ClothingPart</code> classes defines how to draw parts and customization properties
            the part needs, while <code>Clothing</code> just mostly defines which
            <code>ClothingPart</code> parts it has to allow modular composition of parts.
        </p>
        <p>
            <code>this</code> inside <code>renderClothingPoints</code> is the clothing object, which
            inherits all the properties of its clothing parts.
        </p>
    </div>

{% highlight javascript %}
export class ToqueBasePart extends ClothingPart {
    constructor(...data) {
        super({
                layer: Layer.ABOVE_HAIR,
                loc  : "head",
            },
            // properties this part needs for customization
            // they'll be inherited by the clothing object and accessible via `this`
            {
                height      : 4.5,
                sideBias    : -0.15,
                curvature   : 7,
                headCoverage: 0.2,
                sideOffset  : 2.3,
            },
            ...data);
    }
    // other stuff
}
{% endhighlight %}

    <div class="text-block">
        <p>
            It's generally good to factor out calculation of drawpoints into a separate function
            because multiple parts may want to do the same computation. You should call them with
            <code>calcSomething.call(...)</code> instead of directly calling them to forward the
            clothing object as <code>this</code> inside the function.
        </p>
    </div>

{% highlight javascript %}
    renderClothingPoints(ex, ctx) {
        // use .call to forward the clothing object as this to calcToque
        const {rightBot, leftBot, top} = calcToque.call(this, ex);
        Clothes.simpleStrokeFill(ctx, ex, this);

        ctx.beginPath();
        drawPoints(ctx,
            rightBot,
            leftBot,
            top,
            rightBot);
        ctx.fill();
        ctx.stroke();
    }

    // outside class...

export function calcToque(ex) {
    // this should be the clothing object here and we expect it to have certain properties
    const rightBot = adjust(getPointOnCurve(this.headCoverage, ex.skull.side, ex.skull.bot),
        this.sideOffset,
        0);

    // other stuff
}
{% endhighlight %}

<h2 class="anchor">Tips <a class="anchor-link"
                                                title="permalink to section"
                                                href="#tips"
                                                name="tips">&para;</a>
</h2>
<div class="text-block">
    <p>
        To make development more responsive, you should first develop the parts inside
        <code>dist/test.html</code>
        so that changes don't need webpack to rebundle. You'll have to make calls to library code
        with the <code>da.</code> namespace. After you're satisfied, you should move the asset into
        a file inside the <code>src</code> directory and if it's a new file include it in <code>src/index.js</code>.
        When you move it into library code, you have to drop the namespace and instead import them
        (see existing files).
    </p>
    <p>
        This sections will suggest useful functions to use that often provides shortcuts
        for what you're trying to do. For now I'll just list some useful functions you should
        look at.
    </p>
    <ul>
        <li>simpleQuadraticCurve - simplest way to get a natural looking connection of points</li>
        <li>continueCurve (in demo)</li>
        <li>splitCurve - get 2 curves from one</li>
        <li>getPointOnCurve</li>
        <li>adjust - move a point along with its control points</li>
        <li>everything in <code>util/part.js</code></li>
        <li>coverNipplesIfHaveNoBreasts</li>
    </ul>
    <p>
        My approach to creating an asset from scratch is to sort of follow the following algorithm:
    </p>
    <ol>
        <li>sketch out the asset either on paper or in Inkscape</li>
        <li>decide which drawpoints should anchor the part to the body
            (with <code>getPointOnCurve</code> and <code>splitCurve</code>, the anchored body points
        don't have to be drawpoints, but can be any points along the outline of the body)</li>
        <li>start with the minimum number of drawpoints to define the general shape
        (develop an intuition of what a single drawpoint is capable of using the interactive
        drawpoint demo linked above), and define them either relative to each other or to an
        anchoring point on the body</li>
        <li>add more drawpoints to increase detail</li>
    </ol>
</div>

</div>