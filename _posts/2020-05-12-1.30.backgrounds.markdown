---
layout: post
title:  "1.30 Backgrounds and Fixes"
date:   2020-05-29
categories: release
---

- background color in viewConfig
    - default to transparent
- save as PNG in live tester
    - allows for exporting consistently sized images
- fix live tester GUI responsiveness when changing clothes
- fix random character creation to use updated API
- fix tainting canvases by loading images with cors specified
- use TyepScript version of drawpoint library (and consider moving this library to TS...)
- allow custom drawing on the base layer (e.g. your own HUD)
- add base class to Parts for external projects to better specify APIs
    
## Background Color
The viewer config has new properties for making the background transparent (default up to now),
and assigning it a solid color.

```javascript
        window.viewConfig = {
            /* ... */
            // only useful if transparentBackground is false
            backgroundColor      : "#ffffff",
            transparentBackground: true,
        };
```

## Saving as PNG

The live tester now allows you to save the current drawing as a PNG. Simply select the height
of the exported PNG (keeps aspect ratio) and click `Export to PNG`. It's a what you see is
what you get system, so set the background and so on before exporting.

![vitals](https://i.imgur.com/l6wtVRH.png)

