---
layout: post
title:  "Progress on face"
date:   2016-08-13
categories: preview
---
A preview into the new heads of 1.0.0.
I think it's a definite improvement over the previous system.

![new heads]({{site.baseurl}}/res/facepreview.PNG)
![not sure if new update or just teasing]({{site.baseurl}}/res/facepreview2.PNG)

Some sample head shots to show a bit of the expressive range.

This is compared to version 0.13

![old heads](http://i.imgur.com/CtEEo2P.jpg)

(once they get hair they'll look a lot better than the old ones)

Procedurally drawing realistic-looking faces is a lot more difficult than bodies...
You can play around with the new tester [here]({{site.baseurl}}/compare.html)
