---
layout: post
title:  "1.25 Pregnancy"
date:   2019-07-06
categories: release
---

- pregnancy support
    - core statistic
    - belly protrusion dimension

![bdsm](https://i.imgur.com/hQHpbGg.gif)

## Pregnancy

The core statistic `pregnancy` ranges from 0 to 10
(representing the months of pregnancy for humans) and affects
`breastSize`, `areolaSize`, and `bellyProtrusion`.

You can adjust this like other statistics by directly setting it
on the avatar like `pc.pregnancy = 5;`. All characters will start
with it set to 0 by default.

