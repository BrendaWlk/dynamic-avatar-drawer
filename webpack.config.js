const webpack = require('webpack');
const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    context     : path.resolve(__dirname, './src'),
    entry       : {
        da: "./index.js",
    },
    output      : {
        filename     : '[name].js',
        path         : path.resolve(__dirname, 'dist'),
        publicPath   : '/',
        library      : 'da',
        libraryTarget: 'umd',
    },
    devServer   : {
        contentBase: './dist',
        openPage   : 'test.html'
    },
    optimization: {
        minimizer: [
            new UglifyJSPlugin({
                cache        : true,
                parallel     : true,
                uglifyOptions: {
                    compress: true,
                    ecma    : 6,
                    keep_fnames: true,
                    keep_classnames: true,
                },
                sourceMap    : true
            })

        ]
    },
    watch       : false,
    // devtool     : 'inline-module-source-map',
    module      : {
        rules: [
            {
                test   : /\.js$/,
                include: [/src/],
                use    : {
                    loader : 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development')
            },
            __VERSION__: JSON.stringify(require("./package.json").version),
        })
    ]
};
