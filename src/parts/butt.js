import {Layer} from "../util/canvas";
import {BodyPart} from "./part";
import {
    clamp
} from "drawpoint/dist-esm";

class Butt extends BodyPart {
    constructor(...data) {
        super({
            loc    : "butt",
            reflect: true,
            layer  : Layer.BACK
        }, ...data);
    }
}


export class ButtHuman extends Butt {
    constructor(...data) {
        super(...data);
    }

    getLineWidth(avatar) {
        return clamp(0.5 + avatar.dim.buttFullness * 0.05, 0.5, 1.5);
    }

    calcDrawPoints(ex, mods, calculate) {

        if (calculate) {
            ex.butt = {};
            ex.butt.in = {
                x: 0.5,
                y: ex.groin.y - this.buttFullness * 0.01,
            };

            if (!ex.thigh.out) {
                return [];
            }
            ex.butt.out = {
                x: ex.thigh.out.x - 2,
                y: ex.butt.in.y + 1,
            };
            ex.butt.out.cp1 = {
                x: ex.butt.in.x * 0.7 + ex.butt.out.x * 0.3 - this.buttFullness * 0.10,
                y: ex.butt.in.y - this.buttFullness * 0.35
            };
            ex.butt.out.cp2 = {
                x: ex.butt.in.x * 0.3 + ex.butt.out.x * 0.7 + this.buttFullness * 0.12,
                y: ex.butt.out.y - 2 - this.buttFullness * 0.2
            };

            ex.butt.in.cp1 = {
                x: ex.butt.out.cp2.x,
                y: ex.butt.out.y + this.buttFullness * 0.2
            };
            ex.butt.in.cp2 = {
                x: ex.butt.in.x,
                y: ex.butt.in.y + 3 + this.buttFullness * 0.2
            };
        }

        return [ex.butt.in, ex.butt.out, ex.butt.in];
    }
}
