import {Layer} from "../util/canvas";
import {BodyPart} from "./part";
import {
    adjust,
    clamp,
    simpleQuadratic,
} from "drawpoint/dist-esm";

class Testicles extends BodyPart {
    constructor(...data) {
        super({
            loc         : "testicles",
            layer       : Layer.MALE_GENITALS,
            coverConceal: ["groin", "left leg"],
        }, ...data);
    }
}


export class TesticlesHuman extends Testicles {
    constructor(...data) {
        super({
            reflect: true,
        }, ...data);
    }

    getLineWidth(avatar) {
        return clamp(avatar.getDim("testicleSize") / 60, 0.5, 2);
    }

    stroke() {
        return "inherit";
    }

    calcDrawPoints(ex, mods, calculate) {

        if (calculate) {
            let testicles = ex.testicles = {};

            testicles.center = {
                x: 0,
                y: ex.groin.y + 3
            };

            testicles.top = {
                x: testicles.center.x + 1.5 + this.testicleSize * 0.003,
                y: testicles.center.y + this.testicleSize * 0.006,
            };
            testicles.side = {
                x: testicles.center.x + this.testicleSize * 0.06,
                y: testicles.center.y - this.testicleSize * 0.1
            };
            testicles.bot = {
                x: testicles.center.x,
                y: testicles.side.y - this.testicleSize * 0.02
            };

            testicles.side.cp1 = {
                x: testicles.top.x,
                y: testicles.top.y - this.testicleSize * 0.04
            };
            testicles.side.cp2 = {
                x: testicles.side.x + this.testicleSize * 0.01,
                y: testicles.side.y + this.testicleSize * 0.05
            };
            testicles.bot.cp1 = simpleQuadratic(testicles.side,
                testicles.bot,
                0.5,
                this.testicleSize * 0.05);
            testicles.center.cp1 = simpleQuadratic(testicles.bot,
                testicles.center,
                0.3,
                this.testicleSize * 0.01);
        }

        return (this.testicleSize > 20) ? [
            ex.testicles.top,
            ex.testicles.side,
            ex.testicles.bot,
            adjust(ex.testicles.center, 0, -4)
        ] : [];
    }
}
