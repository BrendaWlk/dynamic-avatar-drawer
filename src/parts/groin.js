import {ShadingPart} from "../draw/shading_part";
import {Layer} from "../util/canvas";
import {BodyPart} from "./part";
import {
    extractPoint,
    splitCurve,
    simpleQuadratic,
    reflect,
    fillerDefinition,
} from "drawpoint/dist-esm";

class GroinShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "groin",
            layer: Layer.FRONT,
        }, ...data);
    }

    calcDrawPoints(ex) {
        const top = (ex.thigh.fold === undefined)? extractPoint(ex.hip) : ex.thigh.fold;
        let sp = splitCurve(0.2, ex.thigh.top, top);
        const right = sp.left.p2;
        const groin = extractPoint(ex.groin);
        groin.cp1 = simpleQuadratic(right, groin, 0.5, 2);

        sp = splitCurve(0.8, ex.thigh.top, top);
        const left = reflect(sp.left.p2);
        left.cp1 = reflect(sp.left.p2.cp1);
        if (sp.left.p2.cp2) {
            left.cp2 = reflect(sp.left.p2.cp2);
        }

        const thighTop = reflect(ex.thigh.top);

        right.cp1 = simpleQuadratic(left, right, 0.7, -4);
        return [right, groin, thighTop, left, right];
    }
}


class Groin extends BodyPart {
    constructor(...data) {
        super({
            loc         : "groin",
            forcedSide  : null,
            reflect     : true,
            layer       : Layer.FRONT,
            belowParts  : ["parts torso"],
            shadingParts: [GroinShading]
        }, ...data);
    }
}

export class GroinHuman extends Groin {
    constructor(...data) {
        super(...data);
    }

    getLineWidth() {
        return 0;
    }

    calcDrawPoints(ex, mods, calculate) {

        if (calculate) {
            // remaining height...
            const torsoLength = this.height * 0.85 - this.legLength;
            fillerDefinition(ex, "thigh");
            fillerDefinition(ex.thigh, "top", {
                x: 1.6,
                y: ex.hip.y - this.legLength * 0.15,
            });

            ex.groin = {
                x: 0,
                y: ex.thigh.top.y - torsoLength * 0.015
            };
            ex.groin.top = {
                x: ex.thigh.top.x + 5,
                y: ex.thigh.top.y + 1.1,
            };
        }

        // covered by something else, don't show
        return [ex.groin.top, ex.groin, {fillOnly: [ex.neck.nape, ex.groin.top]}];
    }
}

