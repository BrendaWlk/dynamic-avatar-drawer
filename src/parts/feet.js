import {Layer} from "../util/canvas";
import {BodyPart} from "./part";
import {averageQuadratic} from "../util/draw";
import {Materials} from "../materials";

// classic mixin pattern
class Feet extends BodyPart {
    constructor(...data) {
        super({
            loc       : "feet",
            parentPart: "leg",
            layer     : Layer.FRONT,
        }, ...data);
    }
}


export class FeetHuman extends Feet {
    constructor(...data) {
        super(...data);
    }

    calcDrawPoints(ex, mods, calculate) {

        if (calculate) {
            // expects ankle.out to exist
            ex.ankle.outbot = {
                x: ex.ankle.out.x + 0.2,
                y: ex.ankle.out.y - 2.5,
            };
            ex.ankle.outbot.cp1 =
                averageQuadratic(ex.ankle.out, ex.ankle.outbot, 0.5, 0.5, 0.5);

            const toe = ex.toe = ex.toe || {};
            toe.center = {
                x: ex.ankle.out.x * 0.5 + ex.ankle.in.x * 0.5,
                y: 2-this.feetLength * 0.008 + this.legFem * 0.02
            };
            toe.out = {
                x  : toe.center.x + 1 - this.legFem * 0.04 + this.feetWidth * 0.05,
                y  : toe.center.y,
                cp1: {
                    x: ex.ankle.outbot.x,
                    y: ex.ankle.outbot.y
                },
            };
            toe.out.cp2 = {
                x: toe.out.x + this.legFem * 0.02,
                y: toe.out.y + 1
            };

            toe.in = {
                x: toe.center.x - 1.8 - this.legFem * 0.02 - this.feetWidth * 0.03,
                y: toe.out.y - 0.5 - this.feetLength * 0.006,
            };
            toe.in.cp1 = {
                x: toe.out.x + 1,
                y: toe.out.y - 1 - mods.feetBias * 0.1 * (toe.out.y - toe.in.y)
            };
            toe.in.cp2 = {
                x: toe.in.x + 2.5,
                y: toe.in.y - 0.5 - this.legFem * 0.02,
            };

            ex.ankle.inbot = {
                x  : toe.center.x - 4 + this.legFem * 0.03,
                y  : ex.ankle.outbot.y - 0.5 + this.legFem * 0.03,
                cp1: {
                    x: toe.in.x - 1.5,
                    y: toe.in.y + 1.5
                },
            };

            ex.ankle.inbot.cp2 = {
                x: ex.ankle.inbot.x + 1,
                y: ex.ankle.inbot.y - 2,
            };

            // define for leg
            ex.ankle.in.cp1 =
                averageQuadratic(ex.ankle.inbot, ex.ankle.in, 0.3, -0.5, 0.5);
        }

        return [ex.ankle.outbot, ex.toe.out, ex.toe.in, ex.ankle.inbot,];
    }

}

export class HoofHorse extends Feet {
    constructor(...data) {
        super({
            uncoverable: true,
        }, ...data);
    }

    stroke() {
        return Materials.brownFur.stroke;
    }

    fill() {
        return Materials.brownFur.fill;
    }

    calcDrawPoints(ex, mods, calculate) {

        if (calculate) {
            // expects ankle.out to exist
            ex.ankle.outbot = {
                x: ex.ankle.out.x + 0.2,
                y: ex.ankle.out.y - 2.5,
            };
            ex.ankle.outbot.cp1 =
                averageQuadratic(ex.ankle.out, ex.ankle.outbot, 0.5, 0.5, 0.5);

            ex.toe = ex.toe || {};
            ex.toe.out = {
                x  : ex.ankle.outbot.x + 3 - this.legFem * 0.07,
                y  : 1 + this.legFem * 0.02,
                cp1: {
                    x: ex.ankle.outbot.x,
                    y: ex.ankle.outbot.y
                },
            };
            ex.toe.out.cp2 = {
                x: ex.toe.out.x + this.legFem * 0.02,
                y: ex.toe.out.y + 1
            };

            ex.toe.in = {
                x  : ex.toe.out.x - 7 + this.legFem * 0.05,
                y  : ex.toe.out.y - 2,
                cp1: {
                    x: ex.toe.out.x + 1,
                    y: ex.toe.out.y - 1
                }
            };
            ex.toe.in.cp2 = {
                x: ex.toe.in.x + 2.5,
                y: ex.toe.in.y - 0.5 - this.legFem * 0.02,
            };

            ex.ankle.inbot = {
                x  : ex.toe.in.x - 0.5 + this.legFem * 0.02,
                y  : ex.ankle.outbot.y - 0.5 + this.legFem * 0.03,
                cp1: {
                    x: ex.toe.in.x - 1.5,
                    y: ex.toe.in.y + 1.5
                },
            };

            ex.ankle.inbot.cp2 = {
                x: ex.ankle.inbot.x + 1,
                y: ex.ankle.inbot.y - 2,
            };

            // define for leg
            ex.ankle.in.cp1 =
                averageQuadratic(ex.ankle.inbot, ex.ankle.in, 0.3, -0.5, 0.5);

        }

        return [ex.ankle.outbot, ex.toe.out, ex.toe.in, ex.ankle.inbot,];
    }
}
