import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Layer} from "../util/canvas";
import {
    drawPoints,
} from "drawpoint/dist-esm";
import {Location, setStrokeAndFill} from "..";
import {Part} from "../parts/part";
import {Makeup} from "./makeup";

export class LipstickPart extends ClothingPart {
    constructor(...data) {
        super({
                layer     : Layer.BELOW_HAIR,
                loc       : `${Location.LIPS}`,
                reflect   : false,
                aboveParts: ["faceParts lips"]
            }, {
               fill: "#b91100"
            },
            ...data);
    }

    renderClothingPoints(ex, ctx) {
        Clothes.simpleStrokeFill(ctx, ex, this);

        ctx.beginPath();
        drawPoints(ctx,
			ex.lips.top,
            ex.lips.tip,
            ex.lips.out,
            ex.lips.bot,
            ex.lips.bot.top,
            ex.lips.out.in,
            ex.lips.top.bot,
            ex.lips.top,
		);
        ctx.fill();


    }
}

 
export class Lipstick extends Makeup {
   constructor(...data){
        super({
		},...data);
    }

    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: LipstickPart
            },
            {
                side: Part.RIGHT,
                Part: LipstickPart
            },
        ];
    }
}
