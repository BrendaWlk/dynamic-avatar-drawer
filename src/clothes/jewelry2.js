import {Clothes, ClothingPart, Clothing, processClothingPartLocation} from "./clothing";
import {Layer} from "../util/canvas";
import {locateRelativeToDrawpoint, Location} from "..";
import {
    drawPoints,
	drawCircle,
	adjust,
	extractPoint,
	breakPoint,
	reflect,
	splitCurve,
	rad
} from "drawpoint/dist-esm";

import {Piercing,Jewelry} from "./jewelry";

import {
	polar2cartesian
} from "../util/auxiliary";
 
 
 
export class BellyPiercingSimplePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "+torso",
            reflect   : false,
			aboveParts: ["parts torso", "decorativeParts torso"]
        }, {
            radius: 1.3,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);

		const center = ex.bellybutton.bot;
		const originalFill = this.fill;
		
		function circle(center,radius){
			ctx.fillStyle = originalFill;
			let points = drawCircle(center, radius); 
			ctx.beginPath();
			drawPoints(ctx,
				...points
			);
			ctx.fill();	
			
			points = drawCircle( adjust(center,0.3*radius,0.3*radius), radius*0.3); 
			ctx.fillStyle = "white";
			ctx.beginPath();
			drawPoints(ctx,
				...points
			);
			ctx.fill();	
		};
			
		circle(center, this.radius); 
		
    }
}



export class BellyPiercingAdvancedPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "+torso",
            reflect   : false,
			aboveParts: ["parts torso", "decorativeParts torso"]
        }, {
            thickness: 0.5,
			radius: 1.3,
			secondaryRadius: 1,
			distance: 1,
			above: true,
			above2: true,
			bellow: false,
			bellow2: false,
			chain: true,
			belt: false,
			link: false,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);

		const center = ex.bellybutton.bot;
		const originalFill = this.fill;
		
		function circle(center,radius){
			ctx.fillStyle = originalFill;
			let points = drawCircle(center, radius); 
			ctx.beginPath();
			drawPoints(ctx,
				...points
			);
			ctx.fill();	
			
			points = drawCircle( adjust(center,0.3*radius,0.3*radius), radius*0.3); 
			ctx.fillStyle = "white";
			ctx.beginPath();
			drawPoints(ctx,
				...points
			);
			ctx.fill();	
		};
		
		function chain(center,x,y,thickness){
			center = extractPoint(center);
			
			ctx.setLineDash([2,2]);
			ctx.lineWidth = thickness;
			
			let top = adjust(center,x,0);
			let bot = adjust(center,x,y);
						
			ctx.beginPath();
			drawPoints(ctx,top,bot);
			ctx.stroke();
		}
			
			
		if(this.belt){
			let mid = {x:ex.bellybutton.bot.x,y:ex.bellybutton.bot.y};
			
			let temp = splitCurve(0.7,ex.waist,ex.hip);
			
			let left = extractPoint(temp.left.p2);
			left.cp1 = {x:((left.x+mid.x)*0.4),y:ex.hip.y};
				
			let right = reflect(left);
			right.cp1 = reflect(left.cp1);
		
			ctx.setLineDash([2, 2])
			ctx.lineWidth = 0.6;
			ctx.beginPath();
			drawPoints(ctx,mid,left,breakPoint,mid,right);
			ctx.stroke();
		}	
		
		if(this.chain){
			chain(center,0.6,-7,this.thickness)
			chain(center,-0.6,-9,this.thickness)
				 
		};
			
		if(this.link){
			let bot = extractPoint(adjust(center,0,-6 * this.distance));
			ctx.lineWidth = 0.6;
			ctx.setLineDash([2, 2]);			
			ctx.beginPath();
			drawPoints(ctx,center,bot);
			ctx.stroke();
				 
		};
		
		if(this.above){
			circle( 
				adjust(center,0,3.5 * this.distance),  
				0.7 * this.secondaryRadius
			); 
		}
		
		if(this.above2){
			circle( 
				adjust(center,0,6 * this.distance),  
				0.5 * this.secondaryRadius
			); 
		};
			
		if(this.bellow){
			circle( 
				adjust(center,0,-3.5 * this.distance),  
				0.7 * this.secondaryRadius
			); 
		}
		
		if(this.bellow2){
			circle( 
				adjust(center,0,-6 * this.distance),  
				0.5 * this.secondaryRadius
			); 
		};
		
		circle(center, this.radius); 
		
    }
}

export class NipplePiercingPart extends ClothingPart {
    constructor(...data) {
        super({
            /*
			layer     : Layer.GENITALS,
            loc       : "+chest",
            reflect   : true,
			aboveParts: ["parts torso", "decorativeParts torso"]
			*/
			layer     : Layer.GENITALS,
            loc       : "+torso",
            reflect   : false,
			aboveParts: ["parts chest"],
	//		belowParts: ["decorativeParts torso","decorativeParts chest"]
			//aboveParts: ["parts torso", "decorativeParts torso"]
        }, {
            bar: true,
			ring: true,
			radius: 0.6,
			ringRadius: 1.4,
			thickness: 0.3,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		/*ctx.strokeStyle = "navy"
		ctx.beginPath();
		drawPoints(ctx, ex.chest.nipples, ex.collarbone, ex.hip);
		ctx.stroke();	
		*/
		
		const center = extractPoint(ex.chest.nipples);
		if(this.bar){
			circle(ctx, adjust(center,2,0), this.radius, this.fill);	
			circle(ctx, adjust(center,-2,0), this.radius, this.fill);	
		};
		if(this.ring){
			let points = drawCircle(adjust(center,0,-this.ringRadius), this.ringRadius,); 
			ctx.lineWidth = this.thickness;
			ctx.beginPath();
			drawPoints(ctx,
				...points
			);
			//ctx.arc(center.x, center.y, this.radius, rad(0), rad(90));
			ctx.stroke();	
		};
		
    }
}


export class StudPart2 extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.BELOW_HAIR,
            loc       : `${Location.NOSE}`,
            reflect   : false,
            aboveParts: [`faceParts ${Location.NOSE}`],
        }, {
            radius: 0.51,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
         Clothes.simpleStrokeFill(ctx, ex, this);
		 
		const center = locateRelativeToDrawpoint(ex, this.relativeLocation);
        circle(ctx, center, this.radius, this.fill);
	   /*
        ctx.beginPath();
        let points = drawCircle(center, this.radius);
        drawPoints(ctx, ...points);
        ctx.fill();

		points = drawCircle( adjust(center,0.3*this.radius,0.3*this.radius), this.radius*0.3); 
		ctx.fillStyle = "white";
		ctx.beginPath();
		drawPoints(ctx, ...points);
		ctx.fill();	
		*/	
    }
}

export class BridgePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.BELOW_HAIR,
            loc       : `${Location.NOSE}`,
            reflect   : false,
            aboveParts: [`faceParts ${Location.NOSE}`],
        }, {
            radius: 0.4,
			distance: 0.9,

        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
         Clothes.simpleStrokeFill(ctx, ex, this);
		 
		const center = locateRelativeToDrawpoint(ex, this.relativeLocation);
		const originalFill = this.fill;
			
		circle(ctx, polar2cartesian(this.distance, rad(this.rotation), center), this.radius, this.fill )
		circle(ctx, polar2cartesian(this.distance, rad(this.rotation+180), center), this.radius, this.fill )
    }
}

function circle(ctx, center,radius,originalFill){
	if(originalFill){
		ctx.fillStyle = originalFill;
	}
	let points = drawCircle(center, radius); 
	ctx.beginPath();
	drawPoints(ctx,
		...points
	);
	ctx.fill();	
	
	points = drawCircle( adjust(center,0.3*radius,0.3*radius), radius*0.3); 
	ctx.fillStyle = "white";
	ctx.beginPath();
	drawPoints(ctx,
		...points
	);
	ctx.fill();	
};
		
		
/*



*/		
	
export class NipplePiercing extends Jewelry {
    constructor(...data) {
        super({
			clothingLayer: Clothes.Layer.BASE,
        }, ...data);
    }

	stroke() {
        return "blue";
    }
	
    fill() {
        return "red";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: NipplePiercingPart,
            },
        ];
    }
}
	
export class BellyPiercingSimple extends Jewelry {
    constructor(...data) {
        super({
        }, ...data);
    }

	stroke() {
        return "lime";
    }
	
    fill() {
        return "red";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: BellyPiercingSimplePart,
            },
        ];
    }
}

export class BellyPiercingAdvanced extends Jewelry {
    constructor(...data) {
        super({
        }, ...data);
    }

	stroke() {
        return "lime";
    }
	
    fill() {
        return "red";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: BellyPiercingAdvancedPart,
            },
        ];
    }
}

export class StudPiercing2 extends Piercing {
    constructor(...data) {
        super({
            relativeLocation: {
                drawpoint: "nose.out",
                dx       : -3,
                dy       : 1.15
            },
            loc             : `+${Location.NOSE}`,
            requiredParts   : "faceParts",
			 
        },...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: StudPart2,
            },
        ];
    }
}

export class Bridge extends Piercing {
    constructor(...data) {
        super({
            relativeLocation: {
                drawpoint: "nose.top",
                dx       : -0.3,
                dy       : -1.66
            },
            loc             : `+${Location.NOSE}`,
            requiredParts   : "faceParts",
			 
        },...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: BridgePart,
            },
        ];
    }
}

/*	version for eyebrow:

export class Bridge extends Piercing {
    constructor(...data) {
        super({
            relativeLocation: {
                drawpoint: "brow.outbot",
                dx       : -1.5,
                dy       : 0.4
            },
            loc             : `${Location.NOSE}`,
            requiredParts   : "faceParts",
			 
        },...data);
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: BridgePart,
            },
        ];
    }
}
*/