import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Layer} from "../util/canvas";
import {
    drawPoints,
    extractPoint,
    splitCurve,
    adjust,
    reflect,
	drawCircle,
	breakPoint,
} from "drawpoint/dist-esm";

import {NeckAccessory} from "./neck_accessory";

import {
	getLacingPoints,
	findBetween,
} from "../util/auxiliary";


export class ChokerPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "+neck",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],
            belowSameLayerParts: ["torso"],
        }, {  
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);			
 	
		let {topOut,botOut,topIn,botIn} = calcCollar.call(this, ex);
		
		if(this.center){
			topIn.cp1 = {
				y: ((topIn.y - botIn.y) /2)+botIn.y,
				x:4
			};
			ctx.beginPath();
			//ctx.lineWidth = 0.9;
			drawPoints(ctx,
				topIn,
				topOut,
				botOut,
				botIn,
				topIn
			);
			ctx.fill();
			ctx.stroke();
			return;
		}
		
		ctx.beginPath();
		//ctx.lineWidth = 0.9;
		drawPoints(ctx,
			topIn,
			topOut,
			botOut,
			botIn
		);
		ctx.fill();
		ctx.stroke();
		
    }
}


export class NeckCorsetPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "+neck",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],
            belowSameLayerParts: ["torso"],
        }, {
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);			
		
		let {topOut,botOut,topIn,botIn} = calcCollar.call(this, ex);
		
		//open space
		const openSpace=3;
		topIn = {x:openSpace,y:topOut.y};
		botIn = {x:openSpace,y:botOut.y};
		
		ctx.beginPath();
		//ctx.lineWidth = 0.9;
		drawPoints(ctx,
			topIn,
			topOut,
			botOut,
			botIn
		);
		ctx.fill();
		ctx.stroke();
		
		const lacing = getLacingPoints(botIn,topIn,reflect(botIn),reflect(topIn),this.crossings);
		
		ctx.beginPath();
		ctx.strokeStyle = this.highlight;
		ctx.lineWidth = 0.4;
		drawPoints(ctx,...lacing.outer);
		ctx.stroke();		
	
    }
}


export class CollarPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "+neck",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],
            belowSameLayerParts: ["torso"],
        }, {
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);			
 	
		let temp = splitCurve(this.neckCoverage,ex.neck.cusp,ex.neck.top);
		const center = {x:0,y:findBetween(temp.left.p2.y,ex.neck.cusp.y,0.5)-2};
		
		const radius = 4;
		const points = drawCircle({x:0,y:center.y-radius}, radius);
		ctx.beginPath();
		ctx.lineWidth = 0.9;
		ctx.strokeStyle =  this.ring;
		drawPoints(ctx,...points);
		ctx.stroke();
		
		const size=2;
		ctx.beginPath();
		ctx.lineWidth = 0.5;
		ctx.fillStyle =  this.highlight;
		drawPoints(ctx,
			{x:center.x+size,y:center.y+size},
			{x:center.x+size,y:center.y-size},
			{x:center.x-size,y:center.y-size},
			{x:center.x-size,y:center.y+size}
		);
		//ctx.stroke();
		ctx.fill();		
	
    }
}

export class TagPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "+neck",
            reflect            : false,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],
            belowSameLayerParts: ["torso"],
        }, {
			text: "SLUT",
			fontHeight: 8,
			dx: 0,
			dy: 0,
        }, ...data);
    }
	
    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);			
 	
		let {topOut,botOut,topIn,botIn} = calcCollar.call(this, ex);
		
		//rather hacky
		//there doesn't seem to be a simple way to curve the text and the hard way doesn't seem to be cost-effective
		const anchor = reflect(botIn);
		anchor.x -= 3.5 + this.dx/10;
		anchor.y += 1.2 + this.dy/10;
		ctx.font = this.fontHeight+"px Comic Sans MS";
		ctx.fillStyle = this.highlight;
		ctx.fillText(this.text, anchor.x, anchor.y);
		
    }
}


export class ChokerCrossPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "+neck",
            reflect            : true,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],
            belowSameLayerParts: ["torso"],
        }, {  
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);			
 	
		let {topOut,botOut,topIn,botIn} = calcCollar.call(this, ex);
	
		botOut.cp1 = botIn.cp1;
		botOut.cp2 = botIn.cp2;
		
		const lacing = getLacingPoints(topIn,topOut,botIn,botOut,this.crossings);
		
		ctx.beginPath();
		ctx.lineWidth = this.thickness;
		drawPoints(ctx,...lacing.outer, breakPoint, ...lacing.inner);
		ctx.stroke();			
    }
}

function calcCollar(ex){
	let temp = splitCurve(this.neckCoverage,ex.neck.cusp,ex.neck.top);
	let topOut = adjust(temp.left.p2,-this.thickness*0.5,0);
		temp = splitCurve(this.neckBotCoverage,ex.neck.cusp,ex.neck.top);
	let botOut = adjust(temp.left.p2,-this.thickness*0.5,0);
	botOut = extractPoint(botOut);
	let topIn = {x:-0.1,y:topOut.y};
	let botIn = {x:-0.1,y:botOut.y};
	
	//curves:
	botIn.y-=2.2;
	botIn.cp1 = {
		x: 0.5 * (botIn.x + botOut.x),
		y: botIn.y 
	};
	topIn.y-=2.2;
	topOut.cp1 = {
		x: 0.5 * (topOut.x + topIn.x),
		y: topIn.y 
	};
		
	return {
		topOut,
		botOut,
		topIn,
		botIn
	};
}


/**
 * Concrete Clothing classes
 */
export class Choker extends NeckAccessory {
    constructor(...data) {
        super({
            neckCoverage:0.2,
			neckBotCoverage:0,
			center:false,
			thickness: 0.5,
        }, ...data);
    }

	stroke() {
        return "#5c5c5c";
    }
	
	fill() {
        return "#5c5c5c";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: ChokerPart,
            }
        ];
    }
}

export class NeckCorset extends NeckAccessory {
    constructor(...data) {
        super({
			neckCoverage:0.6,
			neckBotCoverage:0,
			crossings:3,
			highlight:"black",
        }, ...data);
    }
	
	stroke() {
        return "#5c5c5c";
    }
	
    fill() {
        return "#5c5c5c";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: NeckCorsetPart,
            }
        ];
    }
}

export class Collar extends NeckAccessory {
    constructor(...data) {
        super({
			neckCoverage:0.3,
			neckBotCoverage:0,
            ring:"hsla(209, 33%, 70%, 1)", 
			highlight:"hsla(209, 8%, 16%, 1)",
			thickness: 0.5,
        }, ...data);
    }

	stroke() {
        return "#5c5c5c";
    }
	
    fill() {
        return "#5c5c5c";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: CollarPart,
            },{
                side: null,
                Part: ChokerPart,
            }
        ];
    }
}

export class TaggedCollar extends NeckAccessory {
    constructor(...data) {
        super({
			neckCoverage:0.27,
			neckBotCoverage:0,
          
			highlight:"white",
			thickness: 0.5,
        }, ...data);
    }

	stroke() {
        return "hsla(321.3,80.2%,50.4%,1.00)";
    }
	
    fill() {
        return "hsla(321.3,80.2%,50.4%,1.00)";
    }

    get partPrototypes() {
        return [
          {
                side: null,
                Part: TagPart,
            },{
                side: null,
                Part: ChokerPart,
            }
        ];
    }
}

export class CrossedChoker extends NeckAccessory {
    constructor(...data) {
        super({
			neckCoverage:0.21,
			neckBotCoverage:0.05,
			crossings:2,
			thickness: 0.5
        }, ...data);
    }
	
	stroke() {
        return "#5c5c5c";
    }
	
    fill() {
        return "#5c5c5c";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: ChokerCrossPart,
            }
        ];
    }
}