import {clone} from "drawpoint/dist-esm";

/**
 * Facial expressions
 * @namespace Expression
 * @memberof module:da
 */
export const Expression = {
    /**
     * Create an expression to a certain degree
     * @memberof module:da
     * @param {object} expression Expression object with modifiers in Mods
     * @param {number} degree How fully the expression should be displayed; > 0
     * @returns {object} The expression to be applied
     */
    create(expression, degree = 1) {
        const newExpression = clone(expression);
        for (let mod in newExpression.Mods) {
            if (newExpression.Mods.hasOwnProperty(mod)) {
                newExpression.Mods[mod] *= degree;
            }
        }
        return newExpression;
    },

    neutral: Object.freeze({
        Mods: {},
    }),

    suspicious: Object.freeze({
        Mods: {
            eyeBotSize: -7,
            browHeight: -2,
            lipBotSize: -20,
            lipTopSize: -8,
            lipWidth  : -25,
        },
    }),

    angry: Object.freeze({
        Mods: {
            browTilt     : 7,
            browCloseness: -3,
            browHeight   : -4,
            eyeBotSize   : 1,
            eyeTopSize   : 1,
            lipCurl      : -6,
        },
    }),

    sad: Object.freeze({
        Mods: {
            browTilt  : -7,
            eyeBotSize: 2,
            eyeTopSize: -1,
            lipCurl   : -5,
            eyeTilt   : -1,
        },
    }),

    surprised: Object.freeze({
        Mods: {
            browTilt  : -5,
            eyeBotSize: 2,
            eyeTopSize: 1,
            lipParting: 15,
            browHeight: 5,
        },
    }),

    mischievous: Object.freeze({
        Mods: {
            lipCurl   : 10,
            eyeBotSize: -4,
            eyeTopSize: -1,
        },
    }),

    happy: Object.freeze({
        Mods: {
            lipCurl   : 12,
            eyeBotSize: -2,
        },
    }),

    sleepy: Object.freeze({
        Mods: {
            eyeTopSize  : -4,
            eyelidHeight: -1,
        },
    }),

    aroused: Object.freeze({
        Mods: {
            eyelidHeight: -2.5,
            lipParting  : 13,
        },
    }),

    bliss: Object.freeze({
        Mods: {
            irisHeight  : 2,
            lipParting  : 20,
            eyelidHeight: -2,
        },
    }),
};
