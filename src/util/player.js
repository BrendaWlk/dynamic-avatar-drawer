/**
 * Get a default properties factory
 * @param {Object} limits Descriptors of properties, each with avg, low, high, stdev, bias
 * @param {Object} discretePool List of valid values for discrete valued properties
 */
export function getDefault(limits, discretePool) {
    function run() {
        const defaults = {};
        for (let p in limits) {
            if (limits.hasOwnProperty(p)) {
                defaults[p] = limits[p].avg;
            }
        }

        if (discretePool) {
            for (let p in discretePool) {
                if (discretePool.hasOwnProperty(p)) {
                    defaults[p] = discretePool[p][0];
                }
            }
        }
        return defaults;
    }

    return run;
}

/**
 * Apply additional modifiers
 * @param sourceMods Modifiers to be modified
 * @param addMods Modifiers to add
 */
export function applyMods(sourceMods, addMods) {
    for (let mod in addMods) {
        if (addMods.hasOwnProperty(mod)) {
            if (sourceMods.hasOwnProperty(mod)) {
                sourceMods[mod] += addMods[mod];
            } else {
                sourceMods[mod] = addMods[mod];
            }
        }
    }
}

/**
 * Remove modifiers
 * @param sourceMods Modifiers to be modified
 * @param addMods Modifiers to remove
 */
export function removeMods(sourceMods, addMods) {
    for (let mod in addMods) {
        if (addMods.hasOwnProperty(mod)) {
            if (sourceMods.hasOwnProperty(mod)) {
                sourceMods[mod] -= addMods[mod];
            }
        }
    }
}

// CREATE CHARACTER
/**
 * Retrieve female bias with global lookup fallback
 * @param {Object} propertyDescriptor Statistical description of property including avg, low, high
 * @param {string} propertyName
 * @returns {number} Female bias
 */
export function getBiasMod(propertyDescriptor, propertyName) {
    // own defined property takes precidence over the globally defined one
    if (propertyDescriptor.hasOwnProperty("bias")) {
        return propertyDescriptor.bias;
    }
    // default to 1 (higher values correlated with higher femininity)
    return 1;
}

/**
 * Create a transformation object that can be passed into transformAndShow
 * @param {Object} object Any object (such as an avatar)
 * @param {function} showTransformation Function to run at each step of the transformation; it will receive percent of completion (up to 1)
 * @param {Object} transformBy Object matching a subset of the structure of object (can miss properties, but can't have extra)
 * @returns {{transform: (function(*): number), showTransformation: *, object: *}}
 */
export function createTransformation(object, showTransformation, transformBy) {
    let completionPercent = 0;

    function transform(percent) {
        let stepPercent = percent;
        if (completionPercent + percent > 1) {
            stepPercent = 1 - completionPercent;
            completionPercent = 1;
        } else {
            completionPercent += percent;
        }

        performTransformation(object, transformBy, stepPercent);
        return completionPercent;
    }

    function performTransformation(entity, modifications, stepPercent) {
        for (let prop in modifications) {
            if (modifications.hasOwnProperty(prop) && entity.hasOwnProperty(prop)) {
                switch (typeof modifications[prop]) {
                case "object":
                    performTransformation(entity[prop], modifications[prop], stepPercent);
                    break;
                case "number":
                    entity[prop] += modifications[prop] * stepPercent;
                    break;
                default:
                    console.log(`unrecognized transformation property type ${typeof modifications[prop]} for ${prop}`);
                }
            }
        }
    }

    // transformation object
    return {
        object,
        transform,
        showTransformation
    };
}

export function transformAndShow(transformation, duration = 5000) {
    let last = null;

    return new Promise((resolve) => {
        function doTransform(timestamp) {
            const delta = (last === null) ? 0 : timestamp - last;
            last = timestamp;
            const stepPercent = delta / duration;

            const transformationCompletion = transformation.transform(stepPercent);
            transformation.showTransformation(transformationCompletion);

            if (transformationCompletion < 1) {
                window.requestAnimationFrame(doTransform);
            } else {
                resolve();
            }
        }

        window.requestAnimationFrame(doTransform);
    });
}
