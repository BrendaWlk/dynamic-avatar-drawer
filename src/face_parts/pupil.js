import {FacePart} from "./face_part";
import {
    breakPoint,
} from "drawpoint/dist-esm";

class Pupil extends FacePart {
    constructor(...data) {
        super({
            loc       : "pupil",
            aboveParts: ["iris"],
        }, ...data);
    }
}

export class PupilHuman extends Pupil {
    constructor(...data) {
        super(...data);
    }

    stroke() {
        return "#000";
    }

    clipStroke(ex) {
        return [ex.eyes.in, ex.eyes.top, ex.eyes.out, ex.eyes.in];
    }

    getLineWidth(avatar) {
        return avatar.Mods.pupilSize * 0.1;
    }

    calcDrawPoints(ex, mods) {
        return [breakPoint, ex.eyes.iris];
    }
}

