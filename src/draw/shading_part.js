import {none} from "drawpoint/dist-esm";
import {Layer} from "../util/canvas";

export const shine = "hsla(0,100%,100%,0.2)";
export const shadingDark = "hsl(0,15%,80%)";
export const shadingMedium = "hsl(0,15%,85%)";
export const shadingLight = "hsl(0,15%,90%)";

export class ShadingPart {
    constructor(...data) {
        Object.assign(this, {
            layer: Layer.FRONT,
        }, ...data);
    }

    stroke() {
        return none;
    }

    fill() {
        return shadingDark;
    }

    getLineWidth() {
        return 0;
    }
}
